ARG VPP_AGENT=ligato/vpp-agent:v3.1.0

FROM golang:1.15.2-alpine3.12 as gobuilder
RUN apk --no-cache add git
ENV PACKAGEPATH=github.com/networkservicemesh/networkservicemesh/
ENV GO111MODULE=on

RUN mkdir /root/networkservicemesh
ADD ["go.mod","/root/networkservicemesh"]
WORKDIR /root/networkservicemesh/
RUN go mod download

ADD [".","/root/networkservicemesh"]
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/slice-gw-endpoint main.go

FROM ${VPP_AGENT} as runtime
COPY --from=gobuilder /go/bin/slice-gw-endpoint /bin/slice-gw-endpoint
COPY ./etc/ /etc/
COPY ./etc/supervisord/supervisord.conf /opt/vpp-agent/dev/supervisor.conf
COPY ./scripts/vpp_host_net_setup.sh /bin/vpp_host_net_setup.sh
RUN rm /opt/vpp-agent/dev/etcd.conf; \
    echo 'Endpoint: "localhost:9113"' > /opt/vpp-agent/dev/grpc.conf ; \
    echo "disabled: true" > /opt/vpp-agent/dev/telemetry.conf; \
    echo "disabled: true" > /opt/vpp-agent/dev/linux-plugin.conf \
    chmod +x /bin/vpp_host_net_setup.sh
