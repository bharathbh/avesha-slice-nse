#!/bin/bash

log () {
    echo "$1" >> /tmp/vpp_host_net_setup.log
}

log "Mechanism type is $MECHANISM_TYPE"

if [ "$MECHANISM_TYPE" != "MEMIF" ]; then
    exit 0
fi

ip link add name vppout type veth peer name vpphost
ip link set dev vppout up
ip link set dev vpphost up
ip addr add 10.255.255.253/24 dev vpphost

sleep 10

vppctl create host-interface name vppout
vppctl set int state host-vppout up
vppctl set int ip address host-vppout 10.255.255.254/24
vppctl ip route add 0.0.0.0/0 via 10.255.255.253
vppctl show int >> /tmp/vpp_host_net_setup.log
